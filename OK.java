import java.util.Scanner;

class Main {  
  public static void main(String args[]) { 
    Scanner scanner = new Scanner(System.in);
    String name, surname;
    System.out.print("กรุณากรอกชื่อ : ");
    name = scanner.nextLine();
    System.out.print("กรุณากรอกนามสกุล : ");
    surname = scanner.nextLine();
    System.out.println("สวัสดี "+name+" "+surname);
  } 
}